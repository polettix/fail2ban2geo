This program takes the output of:

    fail2ban-client status fail2ban

and extracts the list of blocked IPs, then queries them from a local copy of
GeoIP data to see what countries the source IPs were from.

So yes, this can give us an idea of who is trying to brute force into a
server.

Option `--input`/`-i` allows setting the input. By default it's from
STDIN, which is also the case if it set to the string '-'. If the
argument starts with a colon, it's assumed to be the name of a jail
managed by Fail2ban and used to call the command (if the needed jail is
`XXX`):

    fail2ban-client status XXX

Option `--fail2ban` allows setting the executable for `fail2ban-client`,
in case it's named differently or not available in `PATH`. Option
`--sudo` allows calling it with `sudo`.

The translation from an IP address to the country name is performed
thanks to the Perl module `IP::Geolocation::MMDB`, which leverages a DB
File in MaxMind format available for free from MaxMind, see
[https://dev.maxmind.com/geoip/geolite2-free-geolocation-data](https://dev.maxmind.com/geoip/geolite2-free-geolocation-data).

It's possible to point to the local copy of the database file through
option `--dbpath` or its aliases.

The output is usually sorted from the most frequent down to the least
one. Option `--ascending` and its aliases allow reverting this.

Options:

      ascending: sort in ascending order (most frequent last)
                 command-line: boolean option
                               --ascending | --no-ascending
                               --asc | --no-asc
                               -a
                  environment: F2B2G_SORT_ASCENDING
                      default: 0

         dbpath: path to a MaxMind-compatible db file
                 command-line: mandatory string option
                               --dbpath <value>
                               --db <value>
                               -d <value>
                  environment: F2B2G_DBPATH
                      default: /home/poletti/sviluppo/perl/fail2ban2geo/local/city.mmdb

       fail2ban: command to run for fail2ban
                 command-line: mandatory string option
                               --fail2ban <value>
                  environment: F2B2G_FAIL2BAN
                      default: fail2ban-client

          input: where to get the input (- => stdin, :XXX => jail XXX)
                 command-line: mandatory string option
                               --input <value>
                               -i <value>
                  environment: F2B2G_INPUT
                      default: -

           sudo: run with sudo
                 command-line: boolean option
                               --sudo | --no-sudo
                  environment: F2B2G_SUDO
                      default: 0

Example (also in [example-output.txt][]):

```
China 254
United States 218
Hong Kong 93
Singapore 72
India 70
Germany 53
Netherlands 36
Indonesia 31
Brazil 30
United Kingdom 28
South Korea 28
Russia 27
Vietnam 24
Canada 14
Japan 13
France 12
Thailand 11
Italy 11
...
```


# COPYRIGHT & LICENSE

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2022 by Flavio Poletti (flavio@polettix.it).
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

[example-output.txt]: https://gitlab.com/polettix/fail2ban2geo/-/blob/main/example-output.txt
